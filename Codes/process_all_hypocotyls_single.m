function process_all_hypocotyls_single(num1, num2)

% We process all the hypocotyls between num1 and num2

total_hypocotyls_measured = 0;
h1 = figure;
h2 = figure;

for i= num1: num2
    filename = strcat('output/Points_data',num2str(i),'.csv');
    filename2 = strcat('output/Curve_data',num2str(i),'.csv');
    if (exist(filename)== 2) && (exist(filename2)== 2) % Check if this number of file actually exists.
        
        [xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old] = read_data_from_single_output_file(filename);
        
        figure(h1)
        plot(arc_length_old(2:98), curv_old, '-o'); hold on; grid on;
        
        figure(h2)
        plot(1:97, curv_old, '-o'); hold on; grid on;
        
        f2_output = csvread(filename2, 1, 0);
        
        pix_fact = f2_output(1);
        initial_length_in_mm = f2_output(2);
        initial_total_curvature = f2_output(3);
        
        initial_lengths(total_hypocotyls_measured+1) = initial_length_in_mm;
        initial_curvatures(total_hypocotyls_measured+1) = initial_total_curvature;
        
        total_hypocotyls_measured = total_hypocotyls_measured + 1;
        
    end
       
end

% grid on
init_length = mean(initial_lengths);
length_dev = std(initial_lengths);


titles = {'lengths', 'total curvature',  'total hypocotyl angle'};
fid = fopen('output/summary.csv', 'w');
fprintf(fid, '%s,', titles{1, 1:end-1});
fprintf(fid, '%s\n', titles{1, end});
fclose(fid);

data_to_write = [initial_lengths', initial_curvatures'];
dlmwrite('output/summary.csv', data_to_write, '-append');


end