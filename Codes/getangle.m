function th = getangle(ux,uy)
    th = zeros(size(ux));
    
    th(ux>0&uy==0) = 0*pi/2;
    th(ux==0&uy>0) = 1*pi/2;
    th(ux<0&uy==0) = 2*pi/2;
    th(ux==0&uy<0) = 3*pi/2;
    
    th(ux>0&uy>0)  =      atan(uy(ux>0&uy>0)./ux(ux>0&uy>0));
    th(ux<0&uy>0)  = pi - atan(uy(ux<0&uy>0)./abs(ux(ux<0&uy>0)));
    th(ux<0&uy<0)  = pi + atan(abs(uy(ux<0&uy<0))./abs(ux(ux<0&uy<0)));
    th(ux>0&uy<0)  =2*pi- atan(abs(uy(ux>0&uy<0))./abs(ux(ux>0&uy<0)));
end