function combine_multiple_plates_same_gene_conditions()

%%%%%%%%%%%%%%%%%%%%
% This file takes output from several folders and creates a spreadsheet
% with all the entries in one single column. It should be used only while
% analysing plates with same genotype under same conditions.
%%%%%%%%%%%%%%%%%%%%

prompt = {'S. No. of first folder', 'S. No. of last folder'};
dlg_title = 'Input';
num_lines = 1;
def = {'1', '5'};

answer = inputdlg(prompt, dlg_title, num_lines, def);

first_folder = str2double(answer(1));
last_folder = str2double(answer(2));

output_file = strcat('output_', num2str(last_folder), '/complete_summary_all_plates.csv');
Titles = {'Lengths (mm)', 'Angles (deg)'};

fid = fopen(output_file, 'w');
fprintf(fid, '%s,', Titles{1, 1:end-1});
fprintf(fid, '%s\n', Titles{1, end});
fclose(fid);

Titles2 = {'', ''};

for i = first_folder: last_folder
    
    png_file_names = strcat('output_', num2str(i), '/*.png');
    d = dir(png_file_names);
    num_of_actual_hypocotyls = length(d);
    
    filename = strcat('output_', num2str(i), '/summary.csv' );
    
    Lengths = csvread(filename, 1, 0, [1, 0, num_of_actual_hypocotyls, 0]);
    Angles = csvread(filename, 1, 1, [1, 1, num_of_actual_hypocotyls, 1]);
    
    % This line adds empty space between the data of two hypocotyls.
    fid = fopen(output_file, 'a');
    fprintf(fid, '%s,', Titles2{1, 1:end-1});
    fprintf(fid, '%s\n', Titles2{1, end});
    fclose(fid);

    data_to_write = [Lengths, Angles];
    dlmwrite(output_file, data_to_write, '-append');
    
end


end