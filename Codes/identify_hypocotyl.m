function [xpf_mod, ypf_mod, length2, x0, y0] = identify_hypocotyl(im)

[gx,gy] = gradient(im); % force field;
[m,n] = size(im);
[x,y] = meshgrid(1:n,1:m);


[x0,y0]=ginputc(3, 'Color', 'r', 'LineWidth', 3);

xpf = x0(1);
ypf = y0(1);
vx = x0(2)-x0(1); vy = y0(2)-y0(1);
vx0 = vx;
vy0 = vy;

r=3;

x_leading = x0(2);
y_leading = y0(2);

distnc = sqrt((x_leading - x0(3))^2 + (y_leading - y0(3))^2);

p=2;
while distnc > 2;
    
    th = getangle(vx,vy);
    alps = linspace(th-pi/2,th+pi/2,100);
    
    if distnc<10; % when near the tip, keep on moving towards it.
        vx = x0(3) - xpf(p-1);
        vy = y0(3) - ypf(p-1);
        th = getangle(vx,vy);
        alps = linspace(th-pi/10, th+pi/10, 30);
    end
    
    if p<7 % initially keep on moving in the direction of second click
        th = getangle(vx0,vy0);
        alps = linspace(th-pi/10, th+pi/10, 30);
    end
    
%     sampling points
    xs = xpf(p-1) + r*cos(alps);
    ys = ypf(p-1) + r*sin(alps);
    
    Is = interp2(x,y,im,xs,ys);
    [val,ind] = min(Is);
    xpf(p) = xpf(p-1) + r*cos(alps(ind));
    ypf(p) = ypf(p-1) + r*sin(alps(ind));
    
    vx = xpf(p) - xpf(p-1);
    vy = ypf(p) - ypf(p-1);
    
    if (p>1000)
        break;
    end
    x_leading = xpf(p);
    y_leading = ypf(p);
    distnc = sqrt((x_leading - x0(3))^2 + (y_leading - y0(3))^2);
    p = p+1;
    

end

% now make it so that all the arrays have a length of 100.
lenX = length(xpf);
q_array = [linspace(1, lenX, 100)]' ;
xpf_new = interp1([1:lenX]', xpf, q_array);
ypf_new = interp1([1:lenX]', ypf, q_array);
xpf_mod = smooth(xpf_new, 20);
ypf_mod = smooth(ypf_new, 20);

% close(h)
% h2 = figure(global_counter);
% imagesc(im); axis equal; colormap gray; %scaling the image data
% imshow(im, 'InitialMagnification',200);  colormap gray;

% num_elem = length(xpf);
% length1 = 0;
length2 = 0;
for i = 2:length(xpf_mod)
%     length1 = length1 + sqrt((xpf(i) - xpf(i-1) )^2 + (ypf(i) - ypf(i-1) )^2);
    length2 = length2 + sqrt((xpf_mod(i) - xpf_mod(i-1) )^2 + (ypf_mod(i) - ypf_mod(i-1) )^2);
end

end