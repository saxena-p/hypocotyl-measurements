function [xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, xpf_mod2, ypf_mod2, tang_x, tang_y, nx, ny, curv, arc_length] = read_data_from_output_file(filename)

Data = csvread(filename, 1, 0);

xpf_mod = Data(:,1);
ypf_mod = Data(:,2);
arc_length_old = Data(:,3);
tangx_old = Data(2:99, 4);
tangy_old = Data(2:99, 5);
nx_old = Data(3:99, 6);
ny_old = Data(3:99, 7);
curv_old = Data(3:99, 8);

xpf_mod2 = Data(:, 9);
ypf_mod2 = Data(:, 10);
arc_length = Data(:, 11);
tang_x = Data(2:99, 12);
tang_y = Data(2:99, 13);
nx = Data(3:99, 14);
ny = Data(3:99, 15);
curv = Data(3:99, 16);

end