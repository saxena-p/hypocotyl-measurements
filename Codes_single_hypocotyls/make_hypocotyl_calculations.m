function [nx, ny, tang_x, tang_y, curv, arc_length, len_array, Total_curvature] = make_hypocotyl_calculations(xpf_mod, ypf_mod)

    len_array = length(xpf_mod);
    arc_length = zeros(len_array, 1);
    curv = zeros(len_array - 3, 1);
    nx = zeros(len_array - 3, 1);
    ny = zeros(len_array - 3, 1);
    tang_x = zeros(len_array - 2, 1);
    tang_y = zeros(len_array - 2, 1);

    ds1 = sqrt( (xpf_mod(2) - xpf_mod(1))^2 + (ypf_mod(2) - ypf_mod(1))^2 );
    arc_length(2) = ds1;

    tang1x = (xpf_mod(2) - xpf_mod(1))/ ds1;
    tang1y = (ypf_mod(2) - ypf_mod(1))/ ds1;
    tang_x(1) = tang1x;
    tang_y(1) = tang1y;
    Total_curvature = 0;

    for i = 1:length(curv)-1

        ds2 = sqrt( (xpf_mod(i+2) - xpf_mod(i+1))^2 + (ypf_mod(i+2) - ypf_mod(i+1))^2 );
        
        arc_length(i+2) = arc_length(i+1) + ds2;

        tang2x = ( xpf_mod(i+2) - xpf_mod(i+1) )/ ds2;
        tang2y = ( ypf_mod(i+2) - ypf_mod(i+1) )/ ds2;

        nx(i) = ( tang2x - tang1x )/ ds2;
        ny(i) = ( tang2y - tang1y )/ ds2;

        curv(i) = sqrt( nx(i)^2 + ny(i)^2 );
        if nx(i) < 0
            curv(i) = - curv(i);
        end

        Total_curvature = Total_curvature + curv(i)*ds2;

        tang1x = tang2x;
        tang1y = tang2y;
        
        tang_x(i+1) = tang2x;
        tang_y(i+1) = tang2y;

    %     ds2 = sqrt( (xpf_mod(3) - xpf_mod(2))^2 + (ypf_mod(3) - ypf_mod(2))^2 );
    end

end