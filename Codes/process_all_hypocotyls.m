function process_all_hypocotyls()
% function process_all_hypocotyls(num1, num2, output_folder_number)

prompt = {'Number of output folder', 'Starting hypocotyl number', 'Last hypocotyl number'};
dlg_title = 'Input';
num_lines = 1;

answer = inputdlg(prompt, dlg_title, num_lines);
output_folder_number = str2double(answer(1));
num1 = str2double(answer(2));
num2 = str2double(answer(3));

% We process all the hypocotyls between num1 and num2

total_hypocotyls_measured = 0;

for i= num1: num2
    filename = strcat('output_', num2str(output_folder_number), '/Points_data',num2str(i),'.csv');
    filename2 = strcat('output_', num2str(output_folder_number), '/Curve_data',num2str(i),'.csv');
    if (exist(filename, 'file')== 2) && (exist(filename2, 'file')== 2) % Check if this number of file actually exists.
        
        [xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, xpf_mod2, ypf_mod2, tang_x, tang_y, nx, ny, curv, arc_length] = read_data_from_output_file(filename);
        
        f2_output = csvread(filename2, 1, 0);
        
        pix_fact = f2_output(1);
        initial_length_in_mm = f2_output(2);
        final_length_in_mm = f2_output(3);
        initial_total_curvature = f2_output(4);
        final_total_curvature = f2_output(5);
        
        initial_lengths(total_hypocotyls_measured+1) = initial_length_in_mm;
        final_lengths(total_hypocotyls_measured+1) = final_length_in_mm;
        initial_curvatures(total_hypocotyls_measured+1) = initial_total_curvature;
        final_curvatures(total_hypocotyls_measured+1) = final_total_curvature;

        total_angle1 = 0;
        total_angle2 = 0;
        for j = 70:95
            if xpf_mod(j+1) == xpf_mod(j)
                angle = 90;
            else
                angle = atan( ( -ypf_mod(j+1) + ypf_mod(j) )/( xpf_mod(j+1) - xpf_mod(j) ) ) * 180/pi;
            end
            
            if angle < 0
                if xpf_mod(j+1) < xpf_mod(j)
                    angle = angle + 180;
                end
            end
            
            total_angle1 = total_angle1 + angle;
            
            % now do the same thing with old hypocotyl
            if xpf_mod2(j+1) == xpf_mod2(j)
                angle = 90;
            else
                angle = atan( ( -ypf_mod2(j+1) + ypf_mod2(j) )/( xpf_mod2(j+1) - xpf_mod2(j) ) ) * 180/pi;
            end
            
            if angle < 0
                if xpf_mod2(j+1) < xpf_mod2(j)
                    angle = angle + 180;
                end
            end
            
            total_angle2 = total_angle2 + angle;
            
        end
        final_angle(total_hypocotyls_measured+1) = total_angle2/26;
        initial_angle(total_hypocotyls_measured+1) = total_angle1/26;
                
        
        total_hypocotyls_measured = total_hypocotyls_measured + 1;
        
    end
       
end

growth_in_length = final_lengths - initial_lengths;
angle_change = final_angle - initial_angle ;

avg_growth_in_length = mean(growth_in_length);
growth_deviation = std(growth_in_length);

avg_change_in_angle = mean(angle_change);
angle_deviation = std(angle_change);

disp(['n = ', num2str(total_hypocotyls_measured)]);
disp([ 'average change in angle is ', num2str(avg_change_in_angle), ' degrees with a deviation of ' num2str(angle_deviation), ' degrees.' ] );
disp([ 'average change in length is ', num2str(avg_growth_in_length), ' mm with a deviation of ' num2str(growth_deviation), ' mm.' ] );
disp([ 'average growth rate is ', num2str(avg_growth_in_length/24), ' mm/hr'] );


output_file = strcat('output_', num2str(output_folder_number), '/summary.csv');

titles = {'initial lengths', 'final lengths', 'initial curvature', 'final curvatures', 'initial hypocotyl angle', 'final hypocotyl angle'};
fid = fopen(output_file, 'w');
fprintf(fid, '%s,', titles{1, 1:end-1});
fprintf(fid, '%s\n', titles{1, end});
fclose(fid);

data_to_write = [initial_lengths', final_lengths', initial_curvatures', final_curvatures', initial_angle', final_angle'];
dlmwrite(output_file, data_to_write, '-append');


end