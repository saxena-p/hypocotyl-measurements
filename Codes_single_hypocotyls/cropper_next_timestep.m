function cropper_next_timestep()

is_interim = exist('interim_img');
if is_interim == 0
    mkdir interim_img
else
    delete interim_img/*
end

DefaultName = strcat(pwd,'/input');
[filename,path]=uigetfile('*.jpg','Get the input image file',DefaultName);
image_name = strcat(path,filename);

% Copy the original image to interim_img folder and save the filename
copyfile(image_name, 'interim_img');
fid = fopen('interim_img/num_img.csv', 'w');
fprintf(fid, '%s\n', filename);
fclose(fid);

% image_name = strcat('input/Image.tif');
im = imread(image_name);
im = sum(im,3); %this would combine RGB into one.
im = im/max(im(:));

msg1 = 'Choose two points on the image on each side to calibrate the pixel to length ratio';
msg2 = 'Left to right is 10.1 cm';
disp(msg1); disp(msg2);

h = figure;
imshow(im, 'InitialMagnification',20);  colormap gray;
[x1,y1]=ginputc(2, 'color', 'r', 'LineWidth', 1);
pix_length = sqrt((x1(1) - x1(2))^2 + (y1(1) - y1(2))^2);
pix_fact = pix_length/101;
close(h)

% Ask the user whether small or large seedlings.
width_choice = menu('Are the seedlings short or long?', 'Short', 'Long');
if width_choice == 1
    
    image_limit_x = 90; %This is how much we go left and right from the user click point.
    image_limit_y = 145;
    
else
    
    image_limit_x = 140; %This is how much we go left and right from the user click point.
    image_limit_y = 245;

end


% Choose the output folder for comparison
folder_name = uigetdir(pwd, 'Choose the folder to compare with');
scaling_clicks_locations_old = csvread(strcat(folder_name, '/hypocotyl_locations_measured.csv'), 0, 0, [0 0 1 1]);
all_hypocotyl_locations_old = csvread(strcat(folder_name, '/hypocotyl_locations_measured.csv'), 2, 0);
% First column is X valus, Second column is Y values.
num_hypocotyls = length(all_hypocotyl_locations_old(:,1));

dlmwrite('interim_img/num_img.csv', num_hypocotyls,'-append','delimiter',',');
dlmwrite('interim_img/num_img.csv', pix_fact,'-append','delimiter',',');

% Now create new hypocotyl locations by comparing the left click point used
% for scaling purpose

all_hypocotyl_locations_new = all_hypocotyl_locations_old;
for i = 1:num_hypocotyls
    % First decide the x-y locations of hypocotyls in the new image.
    all_hypocotyl_locations_new(i,1) = all_hypocotyl_locations_old(i,1) - scaling_clicks_locations_old(1,1) + x1(1);
    all_hypocotyl_locations_new(i,2) = all_hypocotyl_locations_old(i,2) - scaling_clicks_locations_old(1,2) + y1(1);
    
    % Now crop out those hypocotyls and save them.
    img_name = strcat('interim_img/Input_image_', num2str(i), '.tif'); %name of the image to be written
    mid_x = floor(all_hypocotyl_locations_new(i,1));
    mid_y = floor(all_hypocotyl_locations_new(i,2)); % these are the points. Floor converts them to integer.
    
    lu_x = mid_x - image_limit_x;
    lu_y = mid_y - image_limit_y;
    rb_x = mid_x + image_limit_x;
    rb_y = mid_y + image_limit_y;
    
    img_size = size(im);
    if lu_x<1
        lu_x = 1;
    end
    if lu_y < 1
        lu_y = 1;
    end
    if rb_x > img_size(2)
        rb_x = img_size(2);
    end
    if rb_y > img_size(1)
        rb_y = img_size(1);
    end
    
    
    img_mat = im(lu_y : rb_y, lu_x: rb_x);
    imwrite(img_mat, img_name);
    
    
    
end

% Save the template image.
h = figure('Visible', 'off');
imshow(im, 'InitialMagnification',20);  colormap gray;
for i = 1:num_hypocotyls
    hold on; text(all_hypocotyl_locations_new(i,1), all_hypocotyl_locations_new(i,2), num2str(i));
end

saveas(h, 'interim_img/template_original_clicks.jpg');
close(h)

% Write these locations down in the hypocotyl locations file
csvwrite('interim_img/hypocotyl_locations.csv', [x1, y1; all_hypocotyl_locations_new]);

end