function combine_data_of_multiple_plates()

%%%%%%%%%%%%%%%%%%%%%%%%
% This file takes data from several plates (each plate should have same
% number of hypocotyls). It then creates a spreadsheet with angles and
% lengths from each one of them. All the output folders being compared
% should have the same number of hypocotyls
%%%%%%%%%%%%%%%%%%%%%%%%

prompt = {'S. No. of first folder', 'S. No. of last folder', 'No. of hypocotyls in each folder'};
dlg_title = 'Input';
num_lines = 1;
def = {'1', '5', '40'};

answer = inputdlg(prompt, dlg_title, num_lines, def);

first_folder = str2double(answer(1));
last_folder = str2double(answer(2));
num_hypocotyls = str2double(answer(3));

num_plates_to_analyse = last_folder - first_folder + 1;

Angle_data = zeros(num_hypocotyls, num_plates_to_analyse);
Length_data = zeros(num_hypocotyls, num_plates_to_analyse);

Titles = cell(1, 2*num_plates_to_analyse+1);

counter = 0;
for i = first_folder: last_folder
    
    filename = strcat('output_', num2str(i), '/summary.csv' );
    
    counter = counter + 1;
    Length_data(:, counter) = csvread(filename, 1, 0, [1, 0, num_hypocotyls, 0] );
    Angle_data(:, counter) = csvread(filename, 1, 1, [1, 1, num_hypocotyls, 1] );
    
    Titles(counter) = { strcat('Angles_', num2str(counter), ' (deg)' ) };
    Titles(num_plates_to_analyse + 1 + counter) = { strcat('Length_', num2str(counter), ' (mm)' ) };
    
end

output_file = strcat('output_', num2str(i), '/complete_summary_all_plates.csv');

fid = fopen(output_file, 'w');
fprintf(fid, '%s,', Titles{1, 1:end-1});
fprintf(fid, '%s\n', Titles{1, end});
fclose(fid);

data_to_write = [Angle_data, zeros(num_hypocotyls, 1), Length_data];
dlmwrite(output_file, data_to_write, '-append');



end