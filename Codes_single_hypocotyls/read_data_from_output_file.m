function [xpf_mod, ypf_mod, tang_x, tang_y, nx, ny, curv, arc_length] = read_data_from_output_file(filename)

Data = csvread(filename, 1, 0);

xpf_mod = Data(:,1);
ypf_mod = Data(:,2);
arc_length = Data(:,3);
tang_x = Data(2:98, 4);
tang_y = Data(2:98, 5);
nx = Data(3:98, 6);
ny = Data(3:98, 7);
curv = Data(3:98, 8);

end