function [xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old] = read_data_from_single_output_file(filename)

Data = csvread(filename, 1, 0);

xpf_mod = Data(:,1);
ypf_mod = Data(:,2);
arc_length_old = Data(:,3);
tangx_old = Data(2:99, 4);
tangy_old = Data(2:99, 5);
nx_old = Data(3:99, 6);
ny_old = Data(3:99, 7);
curv_old = Data(3:99, 8);

end