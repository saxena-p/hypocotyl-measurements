function batch_processing()

%%%%%%%%%%%%%%%%%%%%%%%%
% This file takes data from several plates (each plate should have same
% number of hypocotyls). It then creates a spreadsheet with angles and
% lengths from each one of them. All the output folders being compared
% should have the same number of hypocotyls
%%%%%%%%%%%%%%%%%%%%%%%%

prompt = {'S. No. of first folder', 'S. No. of last folder', 'Starting hypocotyl number', 'Last hypocotyl number', 'Starting point for angle calculation', 'End point for angle calculation'};
dlg_title = 'Input';
num_lines = 1;
def = {'1', '5', '1', '40', '70', '95'};

% Take the required user input
answer = inputdlg(prompt, dlg_title, num_lines, def);
angle_choice = menu('Angle should be with respect to:', 'Vertical', 'Horizontal');

first_folder_number = str2double(answer(1));
last_folder_number = str2double(answer(2));
num_plates_to_analyse = last_folder_number + 1 - first_folder_number;

num1 = str2double(answer(3));
num2 = str2double(answer(4));
num_hypocotyls = num2 - num1 + 1;
Start_point_angle_calculation = str2double(answer(5));
End_point_angle_calculation = str2double(answer(6));

Angle_data = zeros(num_hypocotyls, num_plates_to_analyse);
Length_data = zeros(num_hypocotyls, num_plates_to_analyse);

Titles = cell(1, 2*num_plates_to_analyse+1);

% Run a loop over all the output folders
counter = 0;
for current_folder_under_operation = first_folder_number: last_folder_number
    counter = counter + 1;
    Titles(counter) = { strcat('Angles_', num2str(counter), ' (deg)' ) };
    Titles(num_plates_to_analyse + 1 + counter) = { strcat('Length_', num2str(counter), ' (mm)' ) };
    
    % Run a loop over all the hypocotyls on this plate
    folder_name = strcat('output_', num2str(current_folder_under_operation));
    total_hypocotyls_measured = 0;

    for i= num1: num2
    
        filename = strcat(folder_name, '/Points_data',num2str(i),'.csv');
        filename2 = strcat(folder_name, '/Curve_data',num2str(i),'.csv');

        if (exist(filename, 'file')== 2) && (exist(filename2, 'file')== 2) % Check if this number of file actually exists.

            [xpf_mod, ypf_mod, ~, ~, ~, ~, ~, ~] = read_data_from_output_file(filename);

            f2_output = csvread(filename2, 1, 0);

            pix_fact = f2_output(1);
            length_in_mm = f2_output(2);

            lengths(total_hypocotyls_measured+1) = length_in_mm;

            total_angle1 = 0;

            for j = Start_point_angle_calculation: End_point_angle_calculation
                if xpf_mod(j+1) == xpf_mod(j)
                    angle = 90;
                else
                    angle = atan( ( -ypf_mod(j+1) + ypf_mod(j) )/( xpf_mod(j+1) - xpf_mod(j) ) ) * 180/pi;
                end

                if xpf_mod(j+1) < xpf_mod(j)
                    angle = angle + 180;
                end

                % Change the angle value if it is to be computed with respect to
                % vertical.
                if angle_choice == 1

                    angle = angle - 90;
    %               % Angle is positive if it is tilted to left and negative if
    %               it is tilted to the right.

                end


                total_angle1 = total_angle1 + angle;

            end
%             angles(total_hypocotyls_measured+1) = total_angle1/(End_point_angle_calculation - Start_point_angle_calculation +1);

            total_hypocotyls_measured = total_hypocotyls_measured + 1;

            Length_data(i, counter) = length_in_mm;
            Angle_data(i, counter) = total_angle1/(End_point_angle_calculation - Start_point_angle_calculation+1 );
             
        end

    
    end
    
end

output_file = strcat('output_', num2str(last_folder_number), '/complete_summary_all_plates.csv');

fid = fopen(output_file, 'w');
fprintf(fid, '%s,', Titles{1, 1:end-1});
fprintf(fid, '%s\n', Titles{1, end});
fclose(fid);

data_to_write = [Angle_data, zeros(num_hypocotyls, 1), Length_data];
dlmwrite(output_file, data_to_write, '-append');

end