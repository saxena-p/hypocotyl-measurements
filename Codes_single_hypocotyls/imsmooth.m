function im = imsmooth(im,iters)
    filsize=1;
    % sd=filsize/2.33;
    % g0=1/(sqrt(2*pi)*sd);
    % g=g0.*exp((-filsize:filsize).^2/(2*sd^2));
    [fx,fy] = meshgrid(-filsize:filsize,-filsize:filsize);
    r = sqrt(fx.*fx+fy.*fy);
    g=exp(-r.^2);
    
    for i = 1:iters
        im = conv2(im,g,'same'); 
    end
    
    im = im/max(im(:));
end