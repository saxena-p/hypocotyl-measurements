function brassica_analyse()

%% Read the input file, resize and display.

DefaultName = strcat(pwd,'/input');
[filename,path]=uigetfile('*.jpg','Get the input image file',DefaultName);
image_name = strcat(path,filename);
im = imread(image_name);
im = sum(im,3); %this would combine RGB into one.
im2 = imresize(im, [600 NaN]);
im2 = im2/max(im2(:));


%% Get the scale
msg1 = 'Choose two points on the image on each side to calibrate the pixel to length ratio';
msg2 = 'Left to right is 10.1 cm';
disp(msg1); disp(msg2);

h = figure;
% imagesc(im); axis equal; colormap gray;
imshow(im2);  colormap gray;
[x1,y1]=ginputc(2, 'color', 'r', 'LineWidth', 1);
pix_length = sqrt((x1(1) - x1(2))^2 + (y1(1) - y1(2))^2);
pix_fact = pix_length/101;
close(h);

%% Analyse each seedling.
h = figure;
imshow(im2);

% Plot the points used for scaling.
hold on; plot(x1(1), y1(1), '*', 'color', 'red');
hold on; plot(x1(2), y1(2), '*', 'color', 'red');

num_seedlings2 = inputdlg('Enter the number of hypocotyls you want to choose');
num_seedlings = str2double(num_seedlings2);

Lengths = zeros(num_seedlings, 1);

% Make the output folder
counter = 0;
is_output = 1;
while is_output ~= 0
    
    counter = counter + 1;
    Output_dirname = strcat('output_', num2str(counter));
    is_output = exist(Output_dirname, 'dir'); 
    
end
mkdir(Output_dirname);

global_counter = 1;
while global_counter <= num_seedlings
    
    % ask user to choose points and identify the hypocotyl
    [xpf_mod, ypf_mod, length, x0_old, y0_old] = identify_hypocotyl(im2);
    Lengths(global_counter) = length/pix_fact;
    
    % display the results and ask for user feedback
    hold on; identified_hypocotyl = plot(xpf_mod, ypf_mod, 'g');
    hold on; start_point = plot(x0_old(1), y0_old(1), '*', 'color','blue');
    hold on; end_point = plot(x0_old(3), y0_old(3), '*', 'color','blue');
    
    queststr = ['Do these calculations seem OK?'];
    user_choice = questdlg( queststr,'How to proceed?', 'Yes', 'Redo this image', 'Skip this image','Yes');
    
    switch user_choice
        case 'Yes'
            % do nothing. Let the program to continue
        case 'Redo this image'
            % Restart the global 'for' loop
            set(identified_hypocotyl, 'Visible', 'off');
            set(start_point, 'Visible', 'off');
            set(end_point, 'Visible', 'off');
            continue;
        case 'Skip this image'
            % Continue with the global 'for' loop 
            set(identified_hypocotyl, 'Visible', 'off');
            set(start_point, 'Visible', 'off');
            set(end_point, 'Visible', 'off');
            Lengths(global_counter) = 0;
            global_counter = global_counter + 1;
            continue;
    end
    
    % make tangent, normal, curvature calculations for the hypocotyl
    [nx_old, ny_old, tangx_old, tangy_old, curv_old, arc_length_old, len_array, initial_total_curvature] = make_hypocotyl_calculations(xpf_mod, ypf_mod);
    
    %Now write data into file
    data_file1 = strcat(Output_dirname, '/Points_data',num2str(global_counter),'.csv');
    data_file2 = strcat(Output_dirname, '/Curve_data',num2str(global_counter),'.csv');
    write_on_file(data_file1, data_file2, xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, pix_fact, Lengths(global_counter));
    
    global_counter = global_counter + 1;
end

%% Store the data in output folder

% Save the figure
figurename = strcat(Output_dirname, '/Output_image.png');
saveas(h,figurename,'png')
close(h)

% Write lengths in a file.
Data_File = strcat(Output_dirname, '/Lengths_data.csv');
csvwrite(Data_File, Lengths);

disp(['output stored in the folder ', Output_dirname]);
end