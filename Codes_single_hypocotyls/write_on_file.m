function write_on_file(fname1, fname2, xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, pix_fact, length_in_mm)

% We just add zeros at the front and back of some vectors just to be able
% to write them in the file. This will be taken care of in the program in
% which we read these files.
Tangx_old = [0; tangx_old; 0];
Tangy_old = [0; tangy_old; 0];
Nx_old = [0; 0; nx_old; 0];
Ny_old = [0; 0; ny_old; 0];
Curv_old = [0; 0; curv_old; 0];


% We first write data on the first file
% Write all the titles on the first row. These are only for the human eye
titles = {'x_first', 'y_first', 'Arc_length', 'TangX', 'TangY', 'NormX', 'NormY', 'Curvature'};
fid = fopen(fname1, 'w');
fprintf(fid, '%s,', titles{1, 1:end-1});
fprintf(fid, '%s\n', titles{1, end});
fclose(fid);

% Now write all the other data.
Data_to_write = [xpf_mod, ypf_mod, arc_length_old, Tangx_old, Tangy_old, Nx_old, Ny_old, Curv_old];
dlmwrite(fname1, Data_to_write, '-append');


% Now we write data on the second file
titles = {'pix_fact', 'length'};
fid = fopen(fname2, 'w');
fprintf(fid, '%s,', titles{1, 1:end-1});
fprintf(fid, '%s\n', titles{1, end});
fclose(fid);

% Now write all the other data.
Data_to_write = [pix_fact, length_in_mm];
dlmwrite(fname2, Data_to_write, '-append');

end