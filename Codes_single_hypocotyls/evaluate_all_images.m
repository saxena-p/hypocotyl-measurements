function evaluate_all_images()

% first make the output folder
counter = 0;
is_output = 1;
while is_output ~= 0
    
    counter = counter + 1;
    Output_dirname = strcat('output_', num2str(counter));
    is_output = exist(Output_dirname, 'dir'); 
    
end
mkdir(Output_dirname);

% Copy the template image and original image into the output folder
copyfile('interim_img/template_original_clicks.jpg', Output_dirname);
fid = fopen('interim_img/num_img.csv');
Original_Image = fgetl(fid); % Reading the first line of num_img.csv file
fclose(fid);
copyfile(strcat('interim_img/', Original_Image), Output_dirname);

% Copy the location file to output folder
copyfile('interim_img/hypocotyl_locations.csv', Output_dirname);
copyfile('interim_img/num_img.csv', Output_dirname);

fdata = csvread('interim_img/num_img.csv', 1, 0);
num_img = fdata(1);
pix_fact = fdata(2);


global_counter = 1;
while global_counter <= num_img
    
    % first open, read and display the image
    image_name = strcat('interim_img/Input_image_',num2str(global_counter),'.tif');
    [im, im1, h] = read_img(image_name, global_counter);
    
    % ask user to choose points and identify the first hypocotyl
    [xpf_mod, ypf_mod, length, x0_old, y0_old] = identify_hypocotyl(im);
    length_in_mm = length/pix_fact;
    
    % make tangent, normal, curvature calculations for the first hypocotyl
    [nx_old, ny_old, tangx_old, tangy_old, curv_old, arc_length_old, len_array, initial_total_curvature] = make_hypocotyl_calculations(xpf_mod, ypf_mod);
    
    % display the results of first hypocotyl and ask for user feedback
    hold on; plot(xpf_mod, ypf_mod, 'g');
    hold on; plot(x0_old(1), y0_old(1), '*', 'color','blue');
    hold on; plot(x0_old(3), y0_old(3), '*', 'color','blue');
%     hold on; quiver(xpf_mod(3:len_array-1), ypf_mod(3:len_array-1), nx_old, ny_old)
    
    queststr = ['Do these calculations seem OK?'];
    user_choice = questdlg( queststr,'How to proceed?', 'Yes', 'Redo this image', 'Skip this image','Yes');
    
    switch user_choice
        case 'Yes'
            % do nothing. Let the program to continue
        case 'Redo this image'
            % Restart the global 'for' loop
            close(h)
%             global_counter = global_counter - 1;
            continue;
        case 'Skip this image'
            % Continue with the global 'for' loop 
            close(h)
            global_counter = global_counter + 1;
            continue;
    end
    
     % Now write all the data into the file and save the figure.
    
    % First save the figure
    figurename = strcat(Output_dirname, '/Output_image_',num2str(global_counter));
    saveas(h,figurename,'png')
    close(h)
    
    %Now write data into file
    data_file1 = strcat(Output_dirname, '/Points_data',num2str(global_counter),'.csv');
    data_file2 = strcat(Output_dirname, '/Curve_data',num2str(global_counter),'.csv');
    write_on_file(data_file1, data_file2, xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, pix_fact, length_in_mm);
    
    global_counter = global_counter + 1;

end
disp(['output stored in the folder ', Output_dirname]);