function evaluate_combined_images()

% first make the output folder
counter = 0;
is_output = 1;
while is_output ~= 0
    
    counter = counter + 1;
    Output_dirname = strcat('output_', num2str(counter));
    is_output = exist(Output_dirname, 'dir'); 
    
end
mkdir(Output_dirname);


fdata = csvread('interim_img/num_img.csv');
num_img = fdata(1);
pix_fact = fdata(2);

global_counter = 1;
while global_counter <= num_img
% for global_counter = 1:num_img
    
    % open and read the image
    % ask user to choose points and identify the first hypocotyl
    % make tangent and curvature calculations for the first hypocotyl
    % write this data into a temporary file
    % ask user to choose points and identify the second hypocotyl
    % make tangent and curvature calculations for the second hypocotyl
    % write the complete data of first and second hypocotyl into a file
    
    % first open, read and display the image
    image_name = strcat('interim_img/Input_image_',num2str(global_counter),'.tif');
    [im, im1, h] = read_img(image_name, global_counter);
    
    % ask user to choose points and identify the first hypocotyl
    [xpf_mod, ypf_mod, length, x0_old, y0_old] = identify_hypocotyl(im);
    initial_length_in_mm = length/pix_fact;
    
    % make tangent, normal, curvature calculations for the first hypocotyl
    [nx_old, ny_old, tangx_old, tangy_old, curv_old, arc_length_old, len_array, initial_total_curvature] = make_hypocotyl_calculations(xpf_mod, ypf_mod);
    
    % display the results of first hypocotyl and ask for user feedback
    hold on; plot(xpf_mod, ypf_mod, 'g');
    hold on; plot(x0_old(1), y0_old(1), '*', 'color','blue');
    hold on; plot(x0_old(3), y0_old(3), '*', 'color','blue');
%     hold on; quiver(xpf_mod(3:len_array-1), ypf_mod(3:len_array-1), nx_old, ny_old)
    
    queststr = ['Do these calculations seem OK?'];
    user_choice = questdlg( queststr,'How to proceed?', 'Yes', 'Redo this image', 'Skip this image','Yes');
    
    switch user_choice
        case 'Yes'
            % do nothing. Let the program to continue
        case 'Redo this image'
            % Restart the global 'for' loop
            close(h)
%             global_counter = global_counter - 1;
            continue;
        case 'Skip this image'
            % Continue with the global 'for' loop 
            close(h)
            global_counter = global_counter + 1;
            continue;
    end
    
    close(h)
    
    % Now if the user is happy with the first hypocotyl, let's continue
    % with the second hypocotyl. Repeat more or less similar things, just
    % keeping all the data of the first hypocotyl preserved
    
    skip_image = 0; % This switch will be flipped if second hypocotyl isn't good.
    second_hypocotyl_OK = 0;
    while second_hypocotyl_OK == 0
        
        % First open the image again and display the data of the first
        % hypocotyl
        h = figure(global_counter);
        imagesc(im1); axis equal; colormap gray; %scaling the image data
        imshow(im1, 'InitialMagnification',200);  colormap gray;
        hold on; plot(xpf_mod, ypf_mod, 'g');
        hold on; plot(x0_old(1), y0_old(1), '*', 'color','blue');
        hold on; plot(x0_old(3), y0_old(3), '*', 'color','blue');
%         hold on; quiver(xpf_mod(3:len_array-1), ypf_mod(3:len_array-1), nx_old, ny_old)

        % Identify the second hypocotyl
        [xpf_mod2, ypf_mod2, length2, x0, y0] = identify_hypocotyl(im);
        final_length_in_mm = length2/pix_fact;

        % make tangent, normal, curvature calculations for the first hypocotyl
        [nx, ny, tang_x, tang_y, curv, arc_length, len_array, final_total_curvature] = make_hypocotyl_calculations(xpf_mod2, ypf_mod2);

        % display the results of second hypocotyl and ask for user feedback
        hold on; plot(xpf_mod2, ypf_mod2, 'g');
        hold on; plot(x0(1), y0(1), '*', 'color','blue');
        hold on; plot(x0(3), y0(3), '*', 'color','blue');
%         hold on; quiver(xpf_mod2(3:len_array-1), ypf_mod2(3:len_array-1), nx, ny)

        queststr = ['Do these calculations seem OK?'];
        user_choice = questdlg( queststr,'How to proceed?', 'Yes', 'Redo this image', 'Skip this image','Yes');

        switch user_choice
            case 'Yes'
                % get out of the while loop
                second_hypocotyl_OK = 1;

            case 'Redo this image'
                % Close the figure and stay in the while loop
                close(h)

            case 'Skip this image'
                % Close the image, get out of the while loop, and continue with the global 'for' loop 
                close(h)
                second_hypocotyl_OK = 1;
                skip_image = 1;
        end

    %while loop ends here
    end
    
    if skip_image == 1
        global_counter = global_counter + 1;
        continue;
    end
    
    
    % Now write all the data into the file and save the figure.
    
    % First save the figure
    figurename = strcat(Output_dirname, '/Output_image_',num2str(global_counter));
    saveas(h,figurename,'png')
    close(h)
    
    %Now write data into file
    data_file1 = strcat(Output_dirname, '/Points_data',num2str(global_counter),'.csv');
    data_file2 = strcat(Output_dirname, '/Curve_data',num2str(global_counter),'.csv');
    write_on_file(data_file1, data_file2, xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, xpf_mod2, ypf_mod2, tang_x, tang_y, nx, ny, curv, arc_length, pix_fact, initial_length_in_mm, final_length_in_mm, initial_total_curvature, final_total_curvature);
    
    global_counter = global_counter + 1;

end

end