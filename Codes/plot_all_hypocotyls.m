function plot_all_hypocotyls()
% function plot_all_hypocotyls(output_folder_number)

prompt = {'Output folder number'};
dlg_title = 'Input the values';
num_lines = 1;

answer = inputdlg(prompt,dlg_title,num_lines);
output_folder_number = str2double(answer(1));

    
dirname = strcat('output_', num2str(output_folder_number));
%     D = dir(dirname);
%     num_files = length(D(not([D.isdir])));
%     num = num_files/3;

dirnames = strcat('output_', num2str(output_folder_number), '/*.png');
d = dir(dirnames);
num = length(d);

X1 = zeros(100, num);
Y1 = zeros(100, num);
X2 = zeros(100, num);
Y2 = zeros(100, num);
curv1 = zeros(95, num);
curv2 = zeros(95, num);
arc_length_final = zeros(95, num); % to be able to plot the curvature against length

filenumber = 1;
for j = 1:num

    filename = strcat(dirname, '/Points_data', num2str(filenumber), '.csv');

    while exist(filename, 'file') ~= 2
        filenumber = filenumber + 1;
        filename = strcat(dirname, '/Points_data', num2str(filenumber), '.csv');
    end

    X1(:,j) = csvread(filename, 1, 0, [1, 0, 100, 0]);
    Y1(:,j) = csvread(filename, 1, 1, [1, 1, 100, 1]);
    X2(:,j) = csvread(filename, 1, 8, [1, 8, 100, 8]);
    Y2(:,j) = csvread(filename, 1, 9, [1, 9, 100, 9]);
    curv1(:,j) = csvread(filename, 4, 7, [4, 7, 98, 7]);
    curv2(:,j) = csvread(filename, 4, 15, [4, 15, 98, 15]);
    arc_length_final = csvread(filename, 4, 2, [4, 2, 98, 2]);

    X1(:,j) = X1(:,j) - X1(1,j);
    Y1(:,j) = Y1(:,j) - Y1(1,j);
    X2(:,j) = X2(:,j) - X2(1,j);
    Y2(:,j) = Y2(:,j) - Y2(1,j);

    filenumber = filenumber+1;

end

fig1 = figure;
ax1 = subplot(1,2,1); plot(X1, -Y1); axis equal; grid on;
ax2 = subplot(1,2,2); plot(X2, -Y2); axis equal; grid on;

X_1 = zeros(100,1);
Y_1 = zeros(100,1);
X_2 = zeros(100,1);
Y_2 = zeros(100,1);
Curv_1 = zeros(95, 1);
Curv_2 = zeros(95, 1);
Arc_Length = zeros(95, 1);

for i = 1:100
    X_1(i) = mean(X1(i,:));
    Y_1(i) = mean(Y1(i,:));
    X_2(i) = mean(X2(i,:));
    Y_2(i) = mean(Y2(i,:));
    
    if i < 96
        Curv_1(i) = mean(curv1(i,:));
        Curv_2(i) = mean(curv2(i,:));
        Arc_Length(i) = mean(arc_length_final(i,:));
    end
    
end

fig2 = figure; 
plot(X_1, -Y_1, 'color', 'blue', 'LineWidth', 4); hold on; plot(X_2, -Y_2, 'color', 'red', 'LineWidth', 4); axis equal; grid on;

Curv_1 = smooth(Curv_1, 20);
Curv_2 = smooth(Curv_2, 20);

fig3 = figure; 
plot(Arc_Length(3:90), Curv_1(3:90), 'color', 'blue', 'LineWidth', 2); hold on; plot(Arc_Length(3:90), Curv_2(3:90), 'color', 'red', 'LineWidth', 2); grid on;

figname1 = strcat('output_', num2str(output_folder_number), '/Fig1_All_hypocotyls_together.jpg');
figname2 = strcat('output_', num2str(output_folder_number), '/Fig1_Averaged_hypocotyls.jpg');
figname3 = strcat('output_', num2str(output_folder_number), '/Fig1_Averaged_curvatures.jpg');

figname_1 = strcat('output_', num2str(output_folder_number), '/Fig1_All_hypocotyls_together.fig');
figname_2 = strcat('output_', num2str(output_folder_number), '/Fig1_Averaged_hypocotyls.fig');
figname_3 = strcat('output_', num2str(output_folder_number), '/Fig1_Averaged_curvatures.fig');


saveas(fig1, figname_1);
saveas(fig2, figname_2);
saveas(fig3, figname_3);

saveas(fig1, figname1);
saveas(fig2, figname2);
saveas(fig3, figname3);

close all

end