function process_brassica_hypocotyls()
% function process_all_hypocotyls(num1, num2, output_folder_number)

folder_name = uigetdir(pwd, 'Choose the folder to operate on');

prompt = { 'Starting hypocotyl number', 'Last hypocotyl number', 'Starting point for angle calculation', 'End point for angle calculation'};
dlg_title = 'Input';
num_lines = 1;
def = {'1', '40', '70', '95'};

answer = inputdlg(prompt, dlg_title, num_lines, def);
% output_folder_number = str2double(answer(1));
num1 = str2double(answer(1));
num2 = str2double(answer(2));
Start_point_angle_calculation = str2double(answer(3));
End_point_angle_calculation = str2double(answer(4));

% We process all the hypocotyls between num1 and num2
Length_to_write_mm = zeros(num2, 1);
Angle_to_write_deg = zeros(num2, 1);

% Whether angle should be with respect to vertical or horizontal?
angle_choice = menu('Angle should be with respect to:', 'Vertical', 'Horizontal');
% value of angle_choice. 1 = vertical. 2 = horizontal

file_append_name = folder_name(length(folder_name)-2: length(folder_name));
output_file_measured = strcat(folder_name, '/summary_measured', file_append_name, '.csv');
fid = fopen(output_file_measured, 'w');
fprintf(fid, '%s,', 'lengths (mm)');
fprintf(fid, '%s\n', 'angles (deg)');
fclose(fid);

total_hypocotyls_measured = 0;

for i= num1: num2
%     filename = strcat('output_', num2str(output_folder_number), '/Points_data',num2str(i),'.csv');
%     filename2 = strcat('output_', num2str(output_folder_number), '/Curve_data',num2str(i),'.csv');
    
    filename = strcat(folder_name, '/Points_data',num2str(i),'.csv');
    filename2 = strcat(folder_name, '/Curve_data',num2str(i),'.csv');
    
    
    if (exist(filename, 'file')== 2) && (exist(filename2, 'file')== 2) % Check if this number of file actually exists.
        
        [xpf_mod, ypf_mod, ~, ~, ~, ~, ~, ~] = read_data_from_output_file(filename);
        
        f2_output = csvread(filename2, 1, 0);
        
        pix_fact = f2_output(1);
        length_in_mm = f2_output(2);
        
        lengths(total_hypocotyls_measured+1) = length_in_mm;

        total_angle1 = 0;
        
        for j = Start_point_angle_calculation: End_point_angle_calculation
            if xpf_mod(j+1) == xpf_mod(j)
                angle = 90;
            else
                angle = atan( ( -ypf_mod(j+1) + ypf_mod(j) )/( xpf_mod(j+1) - xpf_mod(j) ) ) * 180/pi;
            end
            
            if xpf_mod(j+1) < xpf_mod(j)
                angle = angle + 180;
            end

            % Change the angle value if it is to be computed with respect to
            % vertical.
            if angle_choice == 1

                angle = angle - 90;
%               % Angle is positive if it is tilted to left and negative if
%               it is tilted to the right.

                    
            end
            
            
            total_angle1 = total_angle1 + angle;
            
        end
        angles(total_hypocotyls_measured+1) = total_angle1/(End_point_angle_calculation - Start_point_angle_calculation +1);
        
        total_hypocotyls_measured = total_hypocotyls_measured + 1;
        
        Length_to_write_mm(i) = length_in_mm;
        Angle_to_write_deg(i) = total_angle1/(End_point_angle_calculation - Start_point_angle_calculation+1 );
        
        % Write the data on to the summary_measured file
        dlmwrite(output_file_measured, [length_in_mm, Angle_to_write_deg(i)] , '-append');
        
    end
    
end

% 
% titles = {'lengths (mm)', 'angles (deg)'};
% fid = fopen(output_file, 'w');
% fprintf(fid, '%s,', titles{1, 1:end-1});
% fprintf(fid, '%s\n', titles{1, end});
% fclose(fid);
% 
% data_to_write = [Length_to_write_mm, Angle_to_write_deg];
% dlmwrite(output_file, data_to_write, '-append');
% 
% disp(['Average length of hypocotyl is ', num2str(mean(lengths)), ' mm.']);
% disp(['Average angle of hypocotyl is ', num2str(mean(angles)), ' degrees.']);

end