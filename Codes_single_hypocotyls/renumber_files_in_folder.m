function renumber_files_in_folder()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file will just renumber all the files in a folder.
% For example, if you'd like hypocotyls 1 -- 20 be renumbered to 33 -- 52, it
% can do that.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

folder_name = uigetdir(pwd, 'Choose the folder to operate on');
d = dir(folder_name);
num_elements = length(d);
disp(['There are in total ', num2str(num_elements), ' items in this folder.' ,folder_name])

png_image_counter = 0; % First we count the number of png images in the folder to get an idea of the number of hypocotyls
for i = 1:num_elements
    % First get the filename and then check the extension
    filename = d(i).name;
    [~,~,extension] = fileparts(filename);
%     if extension=='.png'
    if strcmp(extension, '.png')==1
        png_image_counter = png_image_counter+1;
    end

end

% disp(['The total number of png image files are ', num2str(png_image_counter)])
prompt = {'Desired starting serial number', 'Current last serial number in this folder'};
dlg_title = 'Input these numerical values';
num_lines = 1;
def = {'50', '50'};

user_answer = inputdlg(prompt, dlg_title, num_lines, def);

number_to_add = str2double(user_answer(1)) - 1;
highest_number_image = str2double(user_answer(2));

current_file_index = highest_number_image;
for i = png_image_counter: -1 :1
    fname1 = strcat(folder_name, '/Curve_data', num2str(current_file_index), '.csv');
    while (exist(fname1, 'file') ~= 2)
        current_file_index = current_file_index-1;
        fname1 = strcat(folder_name, '/Curve_data', num2str(current_file_index), '.csv');
    end
    fname2 = strcat(folder_name, '/Points_data', num2str(current_file_index), '.csv');
    fname3 = strcat(folder_name, '/Output_image_', num2str(current_file_index), '.png');
    
    new_file_index = i+ number_to_add;
    new_fname1 = strcat(folder_name, '/Curve_data', num2str(new_file_index), '.csv');
    new_fname2 = strcat(folder_name, '/Points_data', num2str(new_file_index), '.csv');
    new_fname3 = strcat(folder_name, '/Output_image_', num2str(new_file_index), '.png');
    
    movefile(fname1, new_fname1);
    movefile(fname2, new_fname2);
    movefile(fname3, new_fname3);
    
    current_file_index = current_file_index-1;
end

end