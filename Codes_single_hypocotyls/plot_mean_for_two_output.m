function plot_mean_for_two_output()

%%%%%%%%%%%%%%
% This file takes two output folders as input and plots the average
% hypocotyl in the second output folder.
%%%%%%%%%%%%%%

% prompt = {'S. No. of first folder', 'S. No. of second folder'};
% dlg_title = 'Input';
% num_lines = 1;
% def = {'1', '2', };
% 
% answer = inputdlg(prompt, dlg_title, num_lines, def);
% 
% first_folder = str2double(answer(1));
% second_folder = str2double(answer(2));
% 
% dirname1 = strcat('output_', num2str(first_folder));
% dirname2 = strcat('output_', num2str(second_folder));

dirname1 = uigetdir(pwd, 'Choose the first folder to operate on');
dirname2 = uigetdir(pwd, 'Choose the second folder to operate on');

dirnames = strcat(dirname1, '/*.png');
d = dir(dirnames);
num1 = length(d);

dirnames = strcat(dirname2, '/*.png');
d = dir(dirnames);
num2 = length(d);

X1 = zeros(100, num1);
Y1 = zeros(100, num1);

X2 = zeros(100, num2);
Y2 = zeros(100, num2);

filenumber = 1;
for j = 1:num1

    filename = strcat(dirname1, '/Points_data', num2str(filenumber), '.csv');

    while exist(filename, 'file') ~= 2
        filenumber = filenumber + 1;
        filename = strcat(dirname1, '/Points_data', num2str(filenumber), '.csv');
    end

    X1(:,j) = csvread(filename, 1, 0, [1, 0, 100, 0]);
    Y1(:,j) = csvread(filename, 1, 1, [1, 1, 100, 1]);

    X1(:,j) = X1(:,j) - X1(1,j);
    Y1(:,j) = Y1(:,j) - Y1(1,j);

    filenumber = filenumber+1;

end

filenumber = 1;
for j = 1:num2

    filename = strcat(dirname2, '/Points_data', num2str(filenumber), '.csv');

    while exist(filename, 'file') ~= 2
        filenumber = filenumber + 1;
        filename = strcat(dirname2, '/Points_data', num2str(filenumber), '.csv');
    end

    X2(:,j) = csvread(filename, 1, 0, [1, 0, 100, 0]);
    Y2(:,j) = csvread(filename, 1, 1, [1, 1, 100, 1]);

    X2(:,j) = X2(:,j) - X2(1,j);
    Y2(:,j) = Y2(:,j) - Y2(1,j);

    filenumber = filenumber+1;

end

X_1 = zeros(100,1);
Y_1 = zeros(100,1);
X_2 = zeros(100,1);
Y_2 = zeros(100,1);

X1err = zeros(100,1);
Y1err = zeros(100,1);
X2err = zeros(100,1);
Y2err = zeros(100,1);

for i = 1:100
    X_1(i) = mean(X1(i,:));
    Y_1(i) = mean(Y1(i,:));
    X1err(i) = 2* std(X1(i,:))/sqrt(num1);
    Y1err(i) = 2* std(Y1(i,:))/sqrt(num1);
    
    X_2(i) = mean(X2(i,:));
    Y_2(i) = mean(Y2(i,:));
    X2err(i) = 2* std(X2(i,:))/sqrt(num2);
    Y2err(i) = 2* std(Y2(i,:))/sqrt(num2);
end

X1_up = X_1 + X1err;
X1_down = X_1 - X1err;
Y1_up = Y_1 + Y1err;
Y1_down = Y_1 - Y1err;

X2_up = X_2 + X2err;
X2_down = X_2 - X2err;
Y2_up = Y_2 + Y2err;
Y2_down = Y_2 - Y2err;


fig1 = figure;
plot(X_1, -Y_1, 'color', 'blue', 'LineWidth', 4); axis equal; grid on; hold on
plot(X_2, -Y_2, 'color', 'red', 'LineWidth', 4); axis equal; grid on;


fig2 = figure;

plot(X_1, -Y_1, 'color', 'blue', 'LineWidth', 4); axis equal; grid on; hold on
plot(X1_up, -Y_1, 'b:', 'LineWidth', 2); hold on
plot(X1_down, -Y_1, 'b:', 'LineWidth', 2); hold on

plot(X_2, -Y_2, 'color', 'red', 'LineWidth', 4); axis equal; grid on;
plot(X2_up, -Y_2, 'r:', 'LineWidth', 2); hold on
plot(X2_down, -Y_2, 'r:', 'LineWidth', 2); hold on


fig3 = figure;

plot(X_1, -Y_1, 'color', 'blue', 'LineWidth', 4); axis equal; grid on; hold on
plot(X_1, -Y1_up, 'b:', 'LineWidth', 2); hold on
plot(X_1, -Y1_down, 'b:', 'LineWidth', 2); hold on

plot(X_2, -Y_2, 'color', 'red', 'LineWidth', 4); axis equal; grid on;
plot(X_2, -Y2_up, 'r:', 'LineWidth', 2); hold on
plot(X_2, -Y2_down, 'r:', 'LineWidth', 2); hold on


figname1 = strcat(dirname2, '/Fig_mean_comparison_1.jpg');
figname_1 = strcat(dirname2, '/Fig_mean_comparison_1.fig');

figname2 = strcat(dirname2, '/Fig_mean_comparison_2.jpg');
figname_2 = strcat(dirname2, '/Fig_mean_comparison_2.fig');

figname3 = strcat(dirname2, '/Fig_mean_comparison_3.jpg');
figname_3 = strcat(dirname2, '/Fig_mean_comparison_3.fig');


saveas(fig1, figname1);
saveas(fig1, figname_1);

saveas(fig2, figname2);
saveas(fig2, figname_2);

saveas(fig3, figname3);
saveas(fig3, figname_3);

close all

end