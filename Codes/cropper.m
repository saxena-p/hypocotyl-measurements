clc
clear

is_interim = exist('interim_img');
if is_interim == 0
    mkdir interim_img
else
    delete interim_img/*
end

DefaultName = strcat(pwd,'/input');
[filename,path]=uigetfile('*.jpg','Get the input image file',DefaultName);
image_name = strcat(path,filename);
% image_name = strcat('input/Image.tif');
im = imread(image_name);
im = sum(im,3); %this would combine RGB into one.
% im = imsmooth(im,5);
im = im/max(im(:));

h = figure;
% imagesc(im); axis equal; colormap gray;
imshow(im, 'InitialMagnification',20);  colormap gray;

num_img = 4;
num_images = inputdlg('Enter the number of hypocotyls you want to choose');
num_img = str2double(num_images);

%write this number to a file so that Compute_length.m reads it later.
dlmwrite('interim_img/num_img.csv', num_img);

[x0,y0]=ginputc(num_img, 'color', 'r', 'LineWidth', 3);
image_limit_x = 90; %This is how much we go left and right from the user click point.
image_limit_y = 145;

for i = 1:num_img;
    
    img_name = strcat('interim_img/Input_image_', num2str(i), '.tif'); %name of the image to be written
    mid_x = floor(x0(i));
    mid_y = floor(y0(i)); % these are the points chosen by the user. Floor converts them to integer.
    
    lu_x = mid_x - image_limit_x;
    lu_y = mid_y - image_limit_y;
    rb_x = mid_x + image_limit_x;
    rb_y = mid_y + image_limit_y;
    
    img_size = size(im);
    if lu_x<1
        lu_x = 1;
    end
    if lu_y < 1
        lu_y = 1;
    end
    if rb_x > img_size(2)
        rb_x = img_size(2);
    end
    if rb_y > img_size(1)
        rb_y = img_size(1);
    end
    
    
    img_mat = im(lu_y : rb_y, lu_x: rb_x);
    imwrite(img_mat, img_name);
end
close(h);

msg1 = 'Choose two points on the image on each side to calibrate the pixel to length ratio';
msg2 = 'Left to right is 10.1 cm';
disp(msg1); disp(msg2);

h = figure;
% imagesc(im); axis equal; colormap gray;
imshow(im, 'InitialMagnification',20);  colormap gray;
[x0,y0]=ginputc(2, 'color', 'r', 'LineWidth', 3);
pix_length = sqrt((x0(1) - x0(2))^2 + (y0(1) - y0(2))^2);
pix_fact = pix_length/101;
dlmwrite('interim_img/num_img.csv', pix_fact,'-append','delimiter',',');
close(h);

clear