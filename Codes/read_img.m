function [im, im1, h] = read_img(image_name, global_counter)

    im = imread(image_name);
    im1 = im;
    
    im = sum(im,3); %this would combine RGB into one.
    im = imsmooth(im,5);
    h = figure(global_counter);
%     imagesc(im); axis equal; colormap gray; %scaling the image data
    imshow(im1, 'InitialMagnification',200);  colormap gray;

end