function recreate_hypocotyl()

filename = 'output/Points_data1.csv';
[xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv, arc_length] = read_data_from_single_output_file(filename);

len = length(curv);
x_vals = zeros(len, 1);
y_vals = zeros(len, 1);
angles = zeros(len, 1);
angles(1) = pi/2;

for i = 2: len
    ds = arc_length(i) - arc_length(i-1);
    angles(i) = angles(i-1) - ds* curv(i);
    dx = ds* cos( angles(i) );
    dy = ds* sin( angles(i) );
    x_vals(i) = x_vals(i-1) + dx;
    y_vals(i) = y_vals(i-1) + dy;
end


h1 = figure;
plot(x_vals, y_vals, 'LineWidth', 2); grid on; axis equal

h2 = figure;
plot(xpf_mod, -ypf_mod, 'LineWidth', 2); grid on; axis equal


end