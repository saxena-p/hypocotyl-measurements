function evaluate_single_image()

% first make the output folder
is_output = exist('output'); 
if is_output ==0
    mkdir output
else
    delete output/*
end


fdata = csvread('interim_img/num_img.csv');
num_img = fdata(1);
pix_fact = fdata(2);

global_counter = 1;
while global_counter <= num_img
% for global_counter = 1:num_img
    
    % open and read the image
    % ask user to choose points and identify the first hypocotyl
    % make tangent and curvature calculations for the first hypocotyl
    % write the complete data into a file
    
    % first open, read and display the image
    image_name = strcat('interim_img/Input_image_',num2str(global_counter),'.tif');
    [im,~, h] = read_img(image_name, global_counter);
    
    % ask user to choose points and identify the first hypocotyl
    [xpf_mod, ypf_mod, length, x0_old, y0_old] = identify_hypocotyl(im);
    initial_length_in_mm = length/pix_fact;
    
    % make tangent, normal, curvature calculations for the first hypocotyl
    [nx_old, ny_old, tangx_old, tangy_old, curv_old, arc_length_old, len_array, initial_total_curvature] = make_hypocotyl_calculations(xpf_mod, ypf_mod);
    
    % display the results of first hypocotyl and ask for user feedback
    hold on; plot(xpf_mod, ypf_mod, 'g');
    hold on; plot(x0_old(1), y0_old(1), '*', 'color','blue');
    hold on; plot(x0_old(3), y0_old(3), '*', 'color','blue');
    hold on; quiver(xpf_mod(3:len_array-1), ypf_mod(3:len_array-1), nx_old, ny_old)
    
    queststr = ['Do these calculations seem OK?'];
    user_choice = questdlg( queststr,'How to proceed?', 'Yes', 'Redo this image', 'Skip this image','Yes');
    
    switch user_choice
        case 'Yes'
            % do nothing. Let the program to continue
        case 'Redo this image'
            % Restart the global 'while' loop
            close(h)
            continue;
        case 'Skip this image'
            % Continue with the global 'while' loop 
            close(h)
            global_counter = global_counter + 1;
            continue;
    end
    
    
    % Now write all the data into the file and save the figure.
    
    % First save the figure
    figurename = strcat('output/Output_image_',num2str(global_counter));
    saveas(h,figurename,'png')
    close(h)
    
    %Now write data into file
    data_file1 = strcat('output/Points_data',num2str(global_counter),'.csv');
    data_file2 = strcat('output/Curve_data',num2str(global_counter),'.csv');
    write_single_on_file(data_file1, data_file2, xpf_mod, ypf_mod, tangx_old, tangy_old, nx_old, ny_old, curv_old, arc_length_old, pix_fact, initial_length_in_mm, initial_total_curvature);
    
    global_counter = global_counter + 1;
end



end